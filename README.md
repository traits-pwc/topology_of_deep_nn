Paper:

[Topology of deep neural networks (Naitzat et al.) (2020)](https://arxiv.org/abs/2004.06093)

Terminology, etc:
  - [Persistent Homology](https://en.wikipedia.org/wiki/Persistent_homology), more [here](https://epjdatascience.springeropen.com/articles/10.1140/epjds/s13688-017-0109-5) and [here](https://towardsdatascience.com/persistent-homology-with-examples-1974d4b9c3d0)
    (glorious [alpha shapes](https://en.wikipedia.org/wiki/Alpha_shape) [Edelsbrunner] ?)

Original Code:

https://github.com/topnn/topnn_framework

Dependencies:

Python:
  - `conda install tensorflow-gpu=1.15 plotly psutil` (original)
  - `conda install tensorflow-gpu plotly psutil` (current)

Julia: 
  - [Eirene](https://github.com/Eetion/Eirene.jl)
  - `add Eirene Distances JLD Blink PlotlyJS MultivariateStats Colors CSV Hungarian NPZ`
  
  
[PyJulia](https://pyjulia.readthedocs.io/en/latest/):

```
# bind PyCall to selected python environment

julia> ENV["PYTHON"] = "v:\\bin\\[...]\\python.exe"
julia> Pkg.add("PyCall")
julia> Pkg.build("PyCall")  # redundant?

# test

julia> import PyCall         
julia> PyCall.pyprogramname

activated_conda_env_from_above> pip install julia 

# test

>>> from julia import Base
>>> Base.sind(90)
```