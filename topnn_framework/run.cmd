REM Activate suitable conda environment


set EXAMPLE=rings_9
REM For all cases, except circles_type_8
REM 
set DIM=3
REM set DIM=2
set INPUT=./data/%EXAMPLE%
set OUTPUT=./_output/%EXAMPLE%
set MODEL=10_by_25

REM Train
REM python computer.py --pipeline_name train_2_excel_%DIM%D --output %OUTPUT% --input-tf-dataset %INPUT%/%EXAMPLE%.tfrecords --model %MODEL% --activation_type LeakyRelu --trials 80 --learning_rate 0.02 --training_epochs 12000 --summary_freq 1000

REM Calculate Betti numbers
REM python computer.py --pipeline_name excel_2_betti --output %OUTPUT% --input-tf-dataset %INPUT%/%EXAMPLE%.tfrecords --model %MODEL% --activation_type LeakyRelu --trials 80 --cat2  --divisor 4 --neighbors 35 --betti_max 1 --read_excel_from LeakyRelu

REM Visualize (stop here to get GOOD_SAMPLE from %OUTPUT%\%MODEL%\LeakyRelu\good_results.txt)
REM 
set GOOD_SAMPLE=2020-06-14-23-33-27
REM python computer.py --pipeline_name visualize_excel --pretrained  %OUTPUT%/%MODEL%/%GOOD_SAMPLE%
python computer.py --pipeline_name visualize_tfrecords_dataset --input-tf-folder %INPUT% --input-tf-dataset %EXAMPLE%.tfrecords --output-plots-folder %OUTPUT% --dim=%DIM% --output %OUTPUT%
REM python computer.py --pipeline_name visualize_nn_simple_prediction --input-tf-folder %INPUT% --input-tf-dataset %EXAMPLE%.tfrecords --model %MODEL% --output-plots-folder %OUTPUT%